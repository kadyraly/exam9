import React, {Component} from 'react';

import {connect} from 'react-redux';
import './ContactList.css';
import {NavLink} from "react-router-dom";



class ContactList extends Component {

    render () {

        return(
            <div className="Contact">
                <button><NavLink activeClassName="selected" to="/add">Add new contact</NavLink></button>

                <div className="Name">

                    {Object.keys(this.props.contacts).map(contact => {

                        return (
                            <div className="contactName">

                                <strong>{} </strong>


                            </div>
                        )})}
                </div>

            </div>
        )
    }
};

const mapStateToProps = state => {
    return {
        contacts: state.list.contacts,
        name: state.list.name,
        number: state.list.number,
        mail: state.list.mail,
        image: state.list.image
    };
};

const mapDispatchToProps = dispatch => {
    return {

    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ContactList);