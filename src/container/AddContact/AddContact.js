import React, {Component} from 'react';
import './AddContact.css';
import {connect} from "react-redux";
import {contactChange, contactSave} from "../../store/action/addContact";


class AddContact extends Component {


    render () {
        return (
            <div className="add">
                <h4>Name</h4>
                <input type="text" name="name"  value={this.props.contacts.name} onChange={this.props.onContactChange} />
                <h4>Number</h4>
                <input  type="text" name="number"  value={this.props.contacts.number} onChange={this.props.onContactChange} />
                <h4>Mail</h4>
                <input type="text" name="mail"  value={this.props.contacts.mail} onChange={this.props.onContactChange} />
                <h4>Image</h4>
                <input  type="text" name="image"  value={this.props.contacts.image} onChange={this.props.onContactChange} />
                <button onClick={() => this.props.onContactSaved(this.props.value)}>Save</button>
            </div>
        );
    }

}

const mapStateToProps = state => {
    return {
        contacts: state.dd.contacts,

    };
};

const mapDispatchToProps = dispatch => {
    return {
        onContactChange: (e) => dispatch(contactChange(e.target.value)),
        onContactSaved: value => dispatch(contactSave(value)),
    };
};




export default connect(mapStateToProps, mapDispatchToProps)(AddContact);