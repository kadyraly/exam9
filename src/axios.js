import  axios from 'axios';


const instance = axios.create({
    baseURL: 'https://exam9-622ee.firebaseio.com/'

});

export default instance;