import React, { Component } from 'react';
import {Route, Switch} from "react-router";
import Layout from "./component/Layout/Layout";
import ContactList from "./container/ContactList/ContactList";
import AddContact from "./container/AddContact/AddContact";




class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={ContactList} />
                    <Route path="/add" component={AddContact} />
                </Switch>
            </Layout>

        );
    }
}

export default App;
