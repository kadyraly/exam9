import React, {Fragment} from 'react';
import './Layout.css';
import {NavLink} from "react-router-dom";


const Layout = props => {
    return (
        <Fragment>
            <nav className="nav">
                <ul>
                    <li><NavLink activeClassName="selected" to="/" exact>Contacts</NavLink></li>

                </ul>
            </nav>
            <main className="Layout-Content">
                {props.children}
            </main>
        </Fragment>
    )
};

export default Layout;