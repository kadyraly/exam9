import * as actionTypes from '../action/actionTypes';



const initialState = {
    contacts: {},
    name: '',
    number: '',
    mail: '',
    image: ''

};

const reducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.SAVE_CONTACT:
            return {...state, contacts: state.contacts, value: action.value};
        case actionTypes.CHANGE_CONTACT:
            return {...state, value: action.value};

        default:
            return state;
    }

};

export default reducer;