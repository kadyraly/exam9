import * as actionTypes from '../action/actionTypes';



const initialState = {
    contacts: {},
    name: '',
    number: '',
    mail: '',
    image: ''

};

const reducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.ADD_CONTACT:
            return {...state, contacts: action.value};
        default:
            return state;
    }

};

export default reducer;